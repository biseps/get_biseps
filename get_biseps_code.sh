#!/bin/bash

# Download (clone) the entire BiSEPS source-code
# from an online repository e.g. Bitbucket, Github etc


# Validate command-line arguments
if [ "$#" -ne 1 ];
then
    printf "\nError! You must specify a download folder...\n"
    exit 1
fi


# where will we put the code?
DOWNLOAD_FOLDER="$1"


# does $DOWNLOAD_FOLDER exists already?
if [ -d "$DOWNLOAD_FOLDER" ]; then
    printf "\nError: Download folder '$DOWNLOAD_FOLDER' exists already!\n"
    printf "\nExiting Script now...\n"
    exit 1
else
    # ok - create the folder
    printf "\nCreating download folder: $DOWNLOAD_FOLDER\n"
    mkdir "$DOWNLOAD_FOLDER" --parents

    if [ ! -d "$DOWNLOAD_FOLDER" ]; then
        # oh there was a problem creating folder
        printf "\nError: Could not create download folder: $DOWNLOAD_FOLDER\n"
        printf "\nExiting Script now...\n"
        exit 1
    fi
fi



# Where is the code stored on the web?
CLONE_URL='git@bitbucket.org:biseps'


# First ensure that GIT is setup
git --version 2>&1 >/dev/null 
GIT_IS_AVAILABLE=$?

if [ $GIT_IS_AVAILABLE -eq 0 ]; then 
    printf "\nGIT is available.\n"
else
    printf "\nError: GIT is not setup on this machine\n"
    printf "\nExiting Script now...\n"
    exit 1
fi




# Change into the download folder
cd "$DOWNLOAD_FOLDER"
printf "\n\nDownloading BiSEPS source-code to folder: $DOWNLOAD_FOLDER\n\n"


# Now download all the 
# BiSEPS source code
git clone ${CLONE_URL}/help_biseps.git
git clone ${CLONE_URL}/run_biseps.git
git clone ${CLONE_URL}/run_plato.git
git clone ${CLONE_URL}/pop.git
git clone ${CLONE_URL}/scripts.git
git clone ${CLONE_URL}/mag.git
git clone ${CLONE_URL}/plot.git
git clone ${CLONE_URL}/common.git
git clone ${CLONE_URL}/jktebop.git
git clone ${CLONE_URL}/plato_sims.git
git clone ${CLONE_URL}/tables.git


# show what we downloaded
ls -l
printf "\n\nDownloading finished.\n"

